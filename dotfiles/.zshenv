alias g=git
alias c=clear
alias j=jobs
alias less='less -q'

# See patched /var/lib/snapd/desktop/applications/code_code.desktop
alias code='GTK_IM_MODULE=xim code'

export wp_local_docker_sites="$HOME/wp-local-docker-sites"
export wp_plugins="$HOME/wp-local-docker-sites/sitekit-10uplabs-com/wordpress/wp-content/plugins"
export wp_google_site_kit="$HOME/wp-local-docker-sites/sitekit-10uplabs-com/wordpress/wp-content/plugins/google-site-kit"
