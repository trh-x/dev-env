# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

prompt_callback() {
  if [ `jobs | wc -l` -ne 0 ]; then
    echo -n " j:\j"
  fi
}

GIT_PROMPT_ONLY_IN_REPO=1
GIT_PROMPT_SHOW_UNTRACKED_FILES=no
source ~/.bash-git-prompt/gitprompt.sh

# alias jtags="ctags -R app config lib && sed -i '' -E '/^(if|switch|function|module\.exports|it|describe).+language:js$/d' tags"
# alias jtags="ctags -R src && sed -i -E '/^(if|switch|function|module\.exports|it|describe).+language:js$/d' tags"

alias lockscreen='xscreensaver-command -lock'

for repo_path in ~/{git,git-public}/*/*; do
    env_var="$(echo $repo_path | sed 's/.*git-public\///;s/.*git\///' | sed 's/[-/.]/_/g')"
    export $env_var="$repo_path"
done

# sudo docker run -d -p 6379:6379 --name redis-monty redis
# sudo docker run -it --link redis-monty:redis --rm redis redis-cli -h redis -p 6379

calc() {
  echo "scale=8; $@" | bc
}

find_tmpfiles() {
  if [ "$1" == "" ]; then
    echo "Usage: find_tmpfiles <dir>" >&2
    return
  fi
  find "$1" \( -name ".*.sw[onp]" -or -name "*.orig" \)
}

rm_tmpfiles() {
  if [ "$1" == "" ]; then
    echo "Usage: rm_tmpfiles <dir>" >&2
    return
  fi
  find "$1" \( -name ".*.sw[onp]" -or -name "*.orig" \) -exec rm {} \;
}

vim_from_tmpfiles() {
  # TODO: Handle .orig files?
  if [ "$1" == "" ]; then
    echo "Usage: vim_from_tmpfiles <dir>" >&2
    return
  fi
  prev_open_files=$(find_tmpfiles "$1" | sed -r 's/\/\.(.+)\.sw[onp]$/\/\1/')
  rm_tmpfiles "$1"
  vim -p $prev_open_files
}


# Examples of use:
# $ vim_from_git_cmd "git show"
# $ vim_from_git_cmd "git diff a09707ce5576d4813fa056ebf6f809036b6c6bdb"

vim_from_git_cmd() {
  if [ "$1" == "" ]; then
    echo "Usage: vim_from_git_cmd <git_cmd>" >&2
    return
  fi
  vi -p $($1 --name-only --pretty=) -c 'tabdo vnew | execute "r !'"$1"' " . expand("#:p") | set syntax=diff | wincmd x'
}

alias git_rebase_master='git co master && git pull && git co - && git rebase master'
alias g=git
source /usr/share/bash-completion/completions/git
__git_complete g __git_main

alias gp='GIT_PAGER="delta --plus-color=\"#012800\" --minus-color=\"#340001\" --theme=\"Monokai Extended\"" git'
__git_complete gp __git_main

alias j=jobs
alias c=clear

alias fz='fzf --preview "bat --style=numbers --color=always --line-range :500 {}"'

# set -o vi

# export PAGER="/bin/sh -c \"unset PAGER;col -b -x | \
#     vim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' \
#     -c 'map <SPACE> <C-D>' -c 'map b <C-U>' \
#     -c 'nmap K :Man <C-R>=expand(\\\"<cword>\\\")<CR><CR>' -\""
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#export JAVA_HOME=/snap/android-studio/78/android-studio/jre
#export ANDROID_SDK_ROOT=/home/snarf/Android/Sdk

#export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/tools"
export PATH="$PATH:/opt/terraform/bin"

export CHANGE_MINIKUBE_NONE_USER=true

### Rest of file: auto-.nvmrc

# # prompt_callback() - See https://github.com/magicmonty/bash-git-prompt
# prompt_callback() {
#   if [[ "$PWD" == "$OLDPWD" ]]; then
#       return
#   fi
#   if [[ -f ".nvmrc" ]]; then
#       nvm use
#       NVM_DIRTY=true
#   elif [[ $NVM_DIRTY = true ]]; then
#       nvm use default
#       NVM_DIRTY=false
#   fi
# }

# # export PROMPT_COMMAND=enter_directory
find-up () {
    path=$(pwd)
    while [[ "$path" != "" && ! -e "$path/$1" ]]; do
        path=${path%/*}
    done
    echo "$path"
}

cdnvm(){
    cd "$@";
    nvm_path=$(find-up .nvmrc | tr -d '[:space:]')

    # If there are no .nvmrc file, use the default nvm version
    if [[ ! $nvm_path = *[^[:space:]]* ]]; then

        declare default_version;
        default_version=$(nvm version default);

        # If there is no default version, set it to `node`
        # This will use the latest version on your machine
        if [[ $default_version == "N/A" ]]; then
            nvm alias default node;
            default_version=$(nvm version default);
        fi

        # If the current version is not the default version, set it to use the default version
        if [[ $(nvm current) != "$default_version" ]]; then
            nvm use default;
        fi

        elif [[ -s $nvm_path/.nvmrc && -r $nvm_path/.nvmrc ]]; then
        declare nvm_version
        nvm_version=$(<"$nvm_path"/.nvmrc)

        declare locally_resolved_nvm_version
        # `nvm ls` will check all locally-available versions
        # If there are multiple matching versions, take the latest one
        # Remove the `->` and `*` characters and spaces
        # `locally_resolved_nvm_version` will be `N/A` if no local versions are found
        locally_resolved_nvm_version=$(nvm ls --no-colors "$nvm_version" | tail -1 | tr -d '\->*' | tr -d '[:space:]')

        # If it is not already installed, install it
        # `nvm install` will implicitly use the newly-installed version
        if [[ "$locally_resolved_nvm_version" == "N/A" ]]; then
            nvm install "$nvm_version";
        elif [[ $(nvm current) != "$locally_resolved_nvm_version" ]]; then
            nvm use "$nvm_version";
        fi
    fi
}
alias cd='cdnvm'

complete -C /opt/terraform/0.12.18/bin/terraform terraform
complete -C '/usr/local/bin/aws_completer' aws

# tabtab source for packages
# uninstall by removing these lines
[ -f ~/.config/tabtab/__tabtab.bash ] && . ~/.config/tabtab/__tabtab.bash || true

## Loss Prevention API uses Gradle
export GRADLE_USER_HOME=~/.gradle

export MANPAGER="sh -c 'col -bx | bat -l man -p'"
