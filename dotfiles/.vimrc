" let g:autotagTagsDir=system('git rev-parse --git-dir')[:-2]
" let g:TagsFile

execute pathogen#infect()

" URL: http://vim.wikia.com/wiki/Example_vimrc
" Authors: http://vim.wikia.com/wiki/Vim_on_Freenode
" Description: A minimal, but feature rich, example .vimrc. If you are a
"              newbie, basing your first .vimrc on this file is a good choice.
"              If you're a more advanced user, building your own .vimrc based
"              on this file is still a good idea.
 
"------------------------------------------------------------
" Features {{{1
"
" These options and commands enable some very useful features in Vim, that
" no user should have to live without.
 
" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible
 
" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on
 
" Enable syntax highlighting
syntax on
 
 
"------------------------------------------------------------
" Must have options {{{1
"
" These are highly recommended options.
 
" Vim with default settings does not allow easy switching between multiple files
" in the same editor window. Users can use multiple split windows or multiple
" tab pages to edit multiple files, but it is still best to enable an option to
" allow easier switching between files.
"
" One such option is the 'hidden' option, which allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden
 
" Note that not everyone likes working this way (with the hidden option).
" Alternatives include using tabs or split windows instead of re-using the same
" window as mentioned above, and/or either of the following options:
" set confirm
" set autowriteall
 
" Better command-line completion
set wildmenu
 
" Show partial commands in the last line of the screen
set showcmd
 
" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch
 
" Modelines have historically been a source of security vulnerabilities. As
" such, it may be a good idea to disable them and use the securemodelines
" script, <http://www.vim.org/scripts/script.php?script_id=1876>.
" set nomodeline
 
 
"------------------------------------------------------------
" Usability options {{{1
"
" These are options that users frequently set in their .vimrc. Some of them
" change Vim's behaviour in ways which deviate from the true Vi way, but
" which are considered to add usability. Which, if any, of these options to
" use is very much a personal preference, but they are harmless.
 
" Use case insensitive search, except when using capital letters
"set ignorecase
"set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent
 
" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline
 
" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler
 
" Always display the status line, even if only one window is displayed
set laststatus=2
 
" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm
 
" Use visual bell instead of beeping when doing something wrong
set visualbell
 
" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=
 
" Enable use of the mouse for all modes
"set mouse=a
set mouse=n
 
" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2
 
" Display line numbers on the left
"set number
 
" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200
 
" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F9>
 
 
"------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.
 
" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=2
set softtabstop=2
set expandtab
 
" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
"set tabstop=4
 
 
"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings
 
" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$
 
" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>
 
"------------------------------------------------------------

set list
set listchars=tab:\ \ ,trail:·

map <F2> :nohls<CR>
map <F3> :tabprevious<CR>
map <F4> :tabnext<CR>
map <F5> :lclose<CR>
map <F6> :NERDTreeToggle<CR>
map <F7> :BuffergatorToggle<CR>

set foldmethod=syntax
set foldlevelstart=5

set splitbelow
set splitright

autocmd FileType javascript setlocal iskeyword+=$
autocmd FileType html,css setlocal iskeyword+=-

" FIXME: autotag doesn't seem to work - maybe it doesn't work with --tag-relative

" TODO: Tern shortcut key mapping
map <F12> :TernDef<CR>

set grepprg=ag\ $*
let g:ackprg = 'ag --nogroup --nocolor --column'

" ^W-o :only :tabonly - close all other windows/tabs
" s/foo/\U&/g -> all foos to FOO, s/FOO/\L&/ -> FOOs to foo

" http://vim.wikia.com/wiki/Append_output_of_an_external_command
" :command! -nargs=* -complete=shellcmd R new | setlocal buftype=nofile bufhidden=hide noswapfile | r !<args>
:command! -nargs=* -complete=shellcmd R vnew | setlocal buftype=nofile bufhidden=hide noswapfile | r !<args>

" https://til.hashrocket.com/posts/mvekrlaycp-resize-vim-window-to-the-size-of-its-content
" :execute('resize ' . line('$'))

" let $PAGER=''

set backupdir=~/.vim-tmp//
set directory=~/.vim-tmp//
set undodir=~/.vim-tmp//

"colorscheme elflord
"colorscheme slate
colorscheme ron

if has("mouse_sgr")
    set ttymouse=sgr
else
    set ttymouse=xterm2
end

let mapleader=';'

" TODO: Switch to https://github.com/junegunn/fzf.vim from CtrlP
" Ignore some folders and files for CtrlP indexing
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|\.yardoc\|node_modules\|log\|tmp$',
  \ 'file': '\.so$\|\.dat$|\.DS_Store$'
  \ }

"""
" https://jakobgm.com/posts/vim/git-integration/
"""
"" vim-gitgutter
" Update sign column every quarter second
set updatetime=250
" Jump between hunks
nmap <Leader>gn <Plug>(GitGutterNextHunk)  " git next
nmap <Leader>gp <Plug>(GitGutterPrevHunk)  " git previous
" Hunk-add and hunk-revert for chunk staging
nmap <Leader>ga <Plug>(GitGutterStageHunk)  " git add (chunk)
nmap <Leader>gu <Plug>(GitGutterUndoHunk)   " git undo (chunk)
" Open vimagit pane
nnoremap <leader>gs :Magit<CR>       " git status
" Push to remote
nnoremap <leader>gP :! git push<CR>  " git Push
"" vim-fugitive
" Show commits for every source line
nnoremap <Leader>gb :Gblame<CR>  " git blame
"" vim-rhubarb
" Open current line in the browser
"nnoremap <Leader>gb :.Gbrowse<CR>
" Open visual selection in the browser
"vnoremap <Leader>gb :Gbrowse<CR>
" Ack w/out jumping to first result
nnoremap <Leader>a :Ack!<Space>

autocmd FileType typescript nmap <buffer> <Leader>h : <C-u>echo tsuquyomi#hint()<CR>
" autocmd FileType typescript nmap <buffer> <Leader>h : echo tsuquyomi#hint()
autocmd FileType typescript nmap <buffer> <Leader>r :TsuReferences<CR>

"set includeexpr=substitute(v:fname,'\\#','.','g')
set path+=./src

" Scroll bind all window(scrollbind == scb, ! == toggle)
" :tabdo :windo :set scrollbind!

" Unfold all windows
" :tabdo :windo :set foldlevel=99
nnoremap <leader>zr :tabdo :windo :set foldlevel=99<CR>

"" Fix bash aliases
let $BASH_ENV='~/.vim/bash_env'

" fix slow syntax highlight
set regexpengine=2

" fix term to address home/end key mappings in MacOS
set term=xterm-256color
