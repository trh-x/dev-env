#!/bin/bash

files="$@"

i=0
vim_cmd="-c ':set foldlevelstart=20"
for file in $files ; do
    if [ "$(($i % 6))" == "0" ]; then
        vim_cmd="$vim_cmd | tabedit $file"
    elif [ "$(($i % 6))" == "1" ]; then
        vim_cmd="$vim_cmd | vsp $file"
    elif [ "$(($i % 6))" == "2" ]; then
        vim_cmd="$vim_cmd | vsp $file"
    elif [ "$(($i % 6))" == "3" ]; then
        vim_cmd="$vim_cmd | wincmd h | wincmd h | sp $file"
    elif [ "$(($i % 6))" == "4" ]; then
        vim_cmd="$vim_cmd | wincmd l | sp $file"
    elif [ "$(($i % 6))" == "5" ]; then
        vim_cmd="$vim_cmd | wincmd l | sp $file"
    fi
    i=$(($i+1))
done
vim_cmd="$vim_cmd | set foldlevelstart=0 | tabfirst | bw'"

eval vim $vim_cmd
